package com.salesforse.tests;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import com.codeborne.selenide.testng.ScreenShooter;

/**
 * Base class for TestNG-based test classes
 */
@Listeners({ ScreenShooter.class})
public class BaseTest {
	
	@BeforeSuite
	public static void beforeSuite() {
		
		ScreenShooter.captureSuccessfulTests = false;
		
	}
  
}
