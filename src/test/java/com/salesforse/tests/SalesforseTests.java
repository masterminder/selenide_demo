package com.salesforse.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.codeborne.selenide.testng.ScreenShooter;
import com.salesforse.pages.DashboardPage;
import com.salesforse.pages.LoginPage;
import com.salesforse.util.Constants;
import com.salesforse.util.Utils;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.screenshot;

public class SalesforseTests extends BaseTest {

  private LoginPage loginPage;
  private DashboardPage dashboardPage;  
  
  private String accountName = "Account " + Utils.RandomString(5);
  private String contactName = "Contact " + Utils.RandomString(5);
  private String opportunityName = "Op " + Utils.RandomString(5);
  private String date = Utils.getCurrentDate();
  private String stage = "Prospecting";
  
  @BeforeClass
  public void setUpClass() {
	
    loginPage = new LoginPage();
    dashboardPage = new DashboardPage();
	open("/"); 
    loginPage.login(Constants.USERNAME, Constants.PASSWORD); 
  }

  @Test(priority = 0)
  public void testCreateAccount() {
   dashboardPage.getToolbar()
    .clickOnAccountsMenuItem()
   	.clickOnCreateButton()
   	.createAccount(accountName);
  }
   
   @Test(priority = 1)
   public void testCreateContact() {
    dashboardPage.getToolbar()
    	.clickOnContactsMenuItem()
    	.clickOnCreateButton()
    	.createContact(contactName, accountName);
   }
    
    @Test(priority = 2)
    public void testCreateOpportunity() {
     dashboardPage.getToolbar()
     	.clickOnOpportunitiesMenuItem()
     	.clickOnCreateButton()
     	.createOpportunity(opportunityName, accountName, date, stage);      
  }
}
