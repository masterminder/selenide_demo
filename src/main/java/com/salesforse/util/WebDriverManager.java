package com.salesforse.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverManager {
	
	 private static WebDriver driver = null;

	    private WebDriverManager() {
	    }

	    private static WebDriver setBrowser() {

	        WebDriver driver = null;
	        String browser = System.getProperty("webdriver.driver");
	        if (browser.equals("firefox")) {
	            driver = new FirefoxDriver();
	        }
	        if (browser.equals("chrome")) {
	        	driver = new ChromeDriver();
	        }
	    
	        return driver;
	    }
	    
	    public static WebDriver initDriver() {
	        if (driver == null) {
	            driver = setBrowser();
	        }
	        return driver;
	    }

	    public static void closeDriver() {
	        driver.quit();
	        driver = null;
	    }

}
