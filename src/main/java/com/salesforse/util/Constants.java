package com.salesforse.util;

public class Constants {
	
	public static final String USERNAME = "studentonas@gmail.com";
	
	public static final String PASSWORD = "sfAbcd00";
	
	public static final Integer ELEMENT_WAIT_TIMEOUT = 20;
	
    public static final Integer PAGE_LOAD_TIMEOUT = 10;
    
    public static final Integer IMPLICIT_WAIT_TIMEOUT = 5;
}
