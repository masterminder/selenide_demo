package com.salesforse.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Utils {
	
	private static Random random;
    
    public static String RandomString(int length)
    {
        random = new Random();
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        
        return sb.toString();
    }
    
    public static String getCurrentDate() {
        return new SimpleDateFormat("dd/MM/yyyy").format(new Date());
    }
}
