package com.salesforse.pages;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage {
	
	public LoginPage enterUserName(String userName) {
		 $("#username").setValue(userName);
		  return this;
	  }
	
	public LoginPage enterPassword(String password) {
		$("#password").setValue(password);
		  return this;
	  }
	
	public LoginPage clickLogin() {
		$("#Login").click();
		  return this;
	  }
	
	public void login(String userName, String password) {
		enterUserName(userName)
		.enterPassword(password)
		.clickLogin();
		}
	}

