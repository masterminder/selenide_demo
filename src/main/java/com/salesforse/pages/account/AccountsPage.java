package com.salesforse.pages.account;

import com.salesforse.pages.common.CommonElements;
import com.salesforse.pages.components.ToolbarMenu;

public class AccountsPage {
	
	private ToolbarMenu toolbar;
	
	public AddNewAccountPage clickOnCreateButton() {
		CommonElements.NEW_BTN.click();
		return new AddNewAccountPage();
	}
}
