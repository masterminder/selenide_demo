package com.salesforse.pages.account;

import org.openqa.selenium.By;

import com.codeborne.selenide.SelenideElement;
import com.salesforse.pages.components.ToolbarMenu;

import static com.codeborne.selenide.Selenide.$;

public class AddNewAccountPage {
	
	private ToolbarMenu toolbar;

	private SelenideElement accountNameField = $("#acc2");
	
	private SelenideElement saveButton = $(By.name("save"));
	
	public AddNewAccountPage enterAccountName(String accountName) {
		accountNameField.setValue(accountName);
		return this;
	}
	
	public AddNewAccountPage clickOnSaveButton() {
		saveButton.click();
		return this;
	}
	
	public AccountDetailsPage createAccount(String accountName) {
		enterAccountName(accountName)
		.clickOnSaveButton();
		return new AccountDetailsPage();
	}
}
