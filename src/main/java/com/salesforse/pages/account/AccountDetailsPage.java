package com.salesforse.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.salesforse.pages.components.ToolbarMenu;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;

public class AccountDetailsPage {
	
	private ToolbarMenu toolbar;
	
	public AccountDetailsPage clickOnCreateContuctButton() {
		$(By.name("newContact")).click();
		return this;
	}
	
	public AccountDetailsPage accountNameShouldBeDisplayed(String accountName) {
		$(By.tagName("h2")).shouldHave(text(accountName));
		return this;
	}
	
}
