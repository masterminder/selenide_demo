package com.salesforse.pages.components;

import static com.codeborne.selenide.Selenide.$;

import com.salesforse.pages.account.AccountsPage;
import com.salesforse.pages.contact.ContactsPage;
import com.salesforse.pages.opportunity.OpportunitiesPage;

public class ToolbarMenu {
	
	public AccountsPage clickOnAccountsMenuItem() {	
		$("#Account_Tab>a").click();
		return new AccountsPage();		
	}
	
	public ContactsPage clickOnContactsMenuItem() {
		$("#Contact_Tab>a").click();
		return new ContactsPage();
	}
	
	public OpportunitiesPage clickOnOpportunitiesMenuItem() {
		$("#Opportunity_Tab>a").click();
		return new OpportunitiesPage();
	}

}
