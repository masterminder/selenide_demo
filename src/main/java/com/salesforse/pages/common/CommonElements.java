package com.salesforse.pages.common;

import org.openqa.selenium.By;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CommonElements {
	
	public static SelenideElement NEW_BTN = $(By.name("new"));
	
	public static SelenideElement SAVE_BTN = $(By.name("save"));

}
