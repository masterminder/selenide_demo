package com.salesforse.pages.opportunity;

import static com.codeborne.selenide.Selenide.$;

import com.salesforse.pages.common.CommonElements;
import com.salesforse.pages.components.ToolbarMenu;

public class AddNewOpportunityPage {
	
	private ToolbarMenu toolbar;
	
	public AddNewOpportunityPage enterOpportunityName(String oppName) {
	    $("#opp3").setValue(oppName);
		return this;
	}
	
	public AddNewOpportunityPage enterOpportunityDate(String date) {
		 $("#opp9").setValue(date);
		return this;
	}
	
	public AddNewOpportunityPage enterAccountName(String accountName) {
		 $("#opp4").setValue(accountName);
		return this;
	}
	
	public AddNewOpportunityPage selectOpportunityStage(String stage) {
		$("#opp11").selectOptionByValue(stage);
		return this;
	}
	
	public AddNewOpportunityPage clickOnSaveButton() {
		CommonElements.SAVE_BTN.click();
		return this;
	}
	
	public OpportunityDetailsPage createOpportunity(String name, String accountName, String date, String stage) {
		enterOpportunityName(name)
		.enterAccountName(accountName)
		.enterOpportunityDate(date)
		.selectOpportunityStage(stage)
		.clickOnSaveButton();
		return new OpportunityDetailsPage();
	}
}
