package com.salesforse.pages.opportunity;

import com.salesforse.pages.common.CommonElements;

public class OpportunitiesPage {
	
	public AddNewOpportunityPage clickOnCreateButton() {
		CommonElements.NEW_BTN.click();
		return new AddNewOpportunityPage();
	}
}
