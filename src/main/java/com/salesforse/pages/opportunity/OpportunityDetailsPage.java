package com.salesforse.pages.opportunity;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;

import com.salesforse.pages.components.ToolbarMenu;

public class OpportunityDetailsPage {
	
	private ToolbarMenu toolbar;
	
	public OpportunityDetailsPage opportunityNameShouldBeDisplayed(String oppName) {
		$(By.tagName("h2")).shouldHave(text(oppName));
		return this;
	}
	
}

