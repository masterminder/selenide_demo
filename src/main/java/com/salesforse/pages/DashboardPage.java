package com.salesforse.pages;

import static com.codeborne.selenide.Selenide.$;

import com.salesforse.pages.components.ToolbarMenu;

import static com.codeborne.selenide.Condition.*;

public class DashboardPage {
	
	public ToolbarMenu getToolbar() {
		return new ToolbarMenu();
	}
	
	public DashboardPage isOnDashboardPage() {
	      $("#phHeaderLogoImage").shouldBe(visible);
	      return this;
	}
}
