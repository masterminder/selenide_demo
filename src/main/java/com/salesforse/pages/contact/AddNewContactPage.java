package com.salesforse.pages.contact;

import static com.codeborne.selenide.Selenide.$;

import com.salesforse.pages.common.CommonElements;
import com.salesforse.pages.components.ToolbarMenu;

public class AddNewContactPage {
	
	private ToolbarMenu toolbar;
	
	public AddNewContactPage enterLastName(String lastName) {
		$("#name_lastcon2").setValue(lastName);
		return this;
	}
	
	public AddNewContactPage enterAccountName(String accountName) {
		$("#con4").setValue(accountName);
		return this;
	}
	
	public AddNewContactPage clickOnSaveButton() {
		CommonElements.SAVE_BTN.click();
		return this;
	}
	
	public ContactDetailsPage createContact(String lastName, String accountName) {
		enterLastName(lastName)
		.enterAccountName(accountName)
		.clickOnSaveButton();
		return new ContactDetailsPage();
	}
}
