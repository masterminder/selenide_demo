package com.salesforse.pages.contact;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;

import com.salesforse.pages.components.ToolbarMenu;

public class ContactDetailsPage {
	
	private ToolbarMenu toolbar;
	
	public void clickOnCreateOpportunityButton() {
		$(By.name("newOpp")).click();
	}	
	
	public ContactDetailsPage contractNameShouldBeDisplayed(String contractName) {
		$(By.tagName("h2")).shouldHave(text(contractName));
		return this;
	}
	
}
