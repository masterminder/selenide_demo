package com.salesforse.pages.contact;

import com.salesforse.pages.common.CommonElements;

public class ContactsPage {
	
	public AddNewContactPage clickOnCreateButton() {
		CommonElements.NEW_BTN.click();
		return new AddNewContactPage();
}
}
