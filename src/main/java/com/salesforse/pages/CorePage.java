package com.salesforse.pages;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.salesforse.util.Constants;

/**
 * Abstract class representation of a Page in the UI. Page object pattern
 */
public abstract class CorePage {

  protected WebDriver driver;
  
  protected WebDriverWait wait;
  
  protected By byNewBtn = By.name("new");

  /*
   * Constructor injecting the WebDriver interface
   * 
   * @param webDriver
   */
  public CorePage(WebDriver driver) {
    this.driver = getWebDriver();
    wait = new WebDriverWait(driver, Constants.ELEMENT_WAIT_TIMEOUT);
  }

  public String getTitle() {
    return driver.getTitle();
  }
  
  public void SelectFromDropdown(WebElement dropDown, String value)
  {
      Select dropdown = new Select(dropDown);
      if (dropdown.getFirstSelectedOption().getText() != value)
      {
          dropdown.selectByVisibleText(value);
      }
  }
  
  public void ClickCheckbox(WebElement checkbox)
  {
      if (!checkbox.isSelected())
      {
          checkbox.click();
      }
  }
  
  public boolean isItemDisplayed(String itemName) {
	  String xPath = String.format("//a/span[contains(text(),'%s')]", itemName);
	  
	  if (driver.findElements(By.xpath(xPath)).size() > 0)
      {
          if (driver.findElement(By.xpath(xPath)).isDisplayed())
          {
              return true;
          }
          return false;
      }
      return false;
  }
  
  /*
  public WebElement waitForElementToBeClicable(By locator)
  {  
	  FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(250, TimeUnit.MILLISECONDS);
		wait.withTimeout(5, TimeUnit.SECONDS); 

		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver arg0) {
				WebElement element = arg0.findElement(locator);
				if (element.isDisplayed() & element.isEnabled()) {
					return element;
				}
			}
		};

		wait.until(function);		
  }
  */
  
}
